from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
# Importing HttpResponse from django and using it within the index function to return a response once that function is called
#  'request' is a built-in keyword from django that gives us access to the request object which will contain data from the user like input, tokens, etc.
def index(request):
	return HttpResponse("Hello from the views.py file!")